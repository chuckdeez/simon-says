//
//  GameViewController.m
//  Simon Says
//
//  Created by Charles Scholle on 7/20/14.
//  Copyright (c) 2014 Charles Scholle. All rights reserved.
//

#import "GameViewController.h"
#import "InfoViewController.h"

@interface GameViewController ()

@end

@implementation GameViewController

@synthesize scoreLabel;
@synthesize highscoreLabel;

@synthesize greenButton;
@synthesize redButton;
@synthesize yellowButton;
@synthesize blueButton;

@synthesize startStopButton;

@synthesize backgroundImageView;


- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    simonGame = [[GamePlay alloc] initWithScoreLabel:scoreLabel highScoreLabel:highscoreLabel greenButton:greenButton redButton:redButton yellowButton:yellowButton blueButton:blueButton backgroundImageView:backgroundImageView];
    
}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (IBAction)buttonPressed:(id)sender
{
    [simonGame buttonPressed:sender];
    
    if (![simonGame isRunning]) {
        [startStopButton setTitle:@"Start" forState:UIControlStateNormal];
    }
}

- (IBAction)startStop:(id)sender
{
    if ([[startStopButton currentTitle] isEqualToString:@"Start"]) {
        [startStopButton setTitle:@"Stop" forState:UIControlStateNormal];
        
        [simonGame startNewGame];
        
    } else if ([[startStopButton currentTitle] isEqualToString:@"Stop"]) {
        [startStopButton setTitle:@"Start" forState:UIControlStateNormal];
        
        [simonGame forceGameOver];
    }
}

- (IBAction)reset:(id)sender
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Reset highscore?" message:@"Are you sure you want to reset your highscore?" delegate:self cancelButtonTitle:@"Yes" otherButtonTitles:@"No", nil];
    [alert show];
}

- (void) alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex
{
	if (buttonIndex == 0) {
        [simonGame resetHighscore];
	}
}

- (IBAction)info:(id)sender
{
    InfoViewController *infoViewcontroller = [[InfoViewController alloc] initWithNibName:@"InfoViewController" bundle:nil];
    [self presentViewController:infoViewcontroller animated:YES completion:nil];
}


@end
