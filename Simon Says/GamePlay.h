//
//  GamePlay.h
//  Simon Says
//
//  Created by Charles Scholle on 7/27/14.
//  Copyright (c) 2014 Charles Scholle. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <AVFoundation/AVFoundation.h>

@interface GamePlay : NSObject {
    NSMutableArray *currentSequence;
    NSUInteger playerIndex;
    NSUInteger score;
    BOOL gameOver;
    BOOL idle;
    
    UILabel *scoreLabel;
    UILabel *highscoreLabel;
    
    UIButton *greenButton;
    UIButton *redButton;
    UIButton *yellowButton;
    UIButton *blueButton;
    
    UIImageView *backgroundImageView;
}

@property (strong, nonatomic) NSMutableArray *audioPlayers;

- (id)initWithScoreLabel:(UILabel *)sl highScoreLabel:(UILabel *)hsl greenButton:(UIButton *)gb redButton:(UIButton *)rb yellowButton:(UIButton *)yb blueButton:(UIButton *)bb backgroundImageView:(UIImageView *)biv;

- (void)startNewGame;
- (void)forceGameOver;

- (void)buttonPressed:(id)sender;
- (void)resetHighscore;
- (BOOL)isRunning;

@end
