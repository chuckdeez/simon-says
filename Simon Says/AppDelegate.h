//
//  AppDelegate.h
//  Simon Says
//
//  Created by Charles Scholle on 7/16/14.
//  Copyright (c) 2014 Charles Scholle. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;

@end
