//
//  GameViewController.h
//  Simon Says
//
//  Created by Charles Scholle on 7/20/14.
//  Copyright (c) 2014 Charles Scholle. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GamePlay.h"

@interface GameViewController : UIViewController {
    GamePlay *simonGame;
}

@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;
@property (weak, nonatomic) IBOutlet UILabel *highscoreLabel;

@property (weak, nonatomic) IBOutlet UIButton *greenButton;
@property (weak, nonatomic) IBOutlet UIButton *redButton;
@property (weak, nonatomic) IBOutlet UIButton *yellowButton;
@property (weak, nonatomic) IBOutlet UIButton *blueButton;

@property (weak, nonatomic) IBOutlet UIButton *startStopButton;

@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;

- (IBAction)buttonPressed:(id)sender;
- (IBAction)startStop:(id)sender;
- (IBAction)reset:(id)sender;
- (IBAction)info:(id)sender;

@end
